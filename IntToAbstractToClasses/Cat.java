package IntToAbstractToClasses;

public class Cat extends AbstractClass{

    public String actionMyaw;
    public Cat(String actionMyaw) {
        this.actionMyaw = actionMyaw;
    }

    @Override
    public void saySomething() {
        System.out.println(this.actionMyaw);

    }
}
